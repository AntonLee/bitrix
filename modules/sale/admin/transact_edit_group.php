<?
error_reporting(E_ALL);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");


$saleModulePermissions = $APPLICATION->GetGroupRight("sale");
if ($saleModulePermissions == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sale/include.php");
IncludeModuleLangFile(__FILE__);

$errorMessage = "";
$bVarsFromForm = false;

ClearVars();
$IBLOCK_ID = 3;
$tmp = array();
$elem = CIBlockElement::GetList(Array("NAME" => "ASC"), Array('IBLOCK_ID' => $IBLOCK_ID), false, false, array('ID', 'NAME'));
while ($e = $elem->Fetch()) {
    $tmp[$e['ID']] = $e;
}

if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && $saleModulePermissions >= "U" && check_bitrix_sessid()) {

    if (isset($_REQUEST['GENDER']) && strlen($_REQUEST['GENDER']) > 0) {
        if ($_REQUEST['GENDER'] == 'N') {
            $arFilter['PERSONAL_GENDER'] = false;
        } else {
            $arFilter['PERSONAL_GENDER'] = $_REQUEST['GENDER'];
        }
    }
    if (isset($_REQUEST['CVP']) && is_array($_REQUEST['CVP'])) {
        $arFilter['UF_PRISES'] = $_REQUEST['CVP'];
    }
    if (isset($_REQUEST['DATE_REGISTER_FROM']) && strlen($_REQUEST['DATE_REGISTER_FROM']) > 0) {
        $arFilter['DATE_REGISTER_1'] = $_REQUEST['DATE_REGISTER_FROM'];
    }
    if (isset($_REQUEST['DATE_REGISTER_TO']) && strlen($_REQUEST['DATE_REGISTER_TO']) > 0) {
        $arFilter['DATE_REGISTER_2'] = $_REQUEST['DATE_REGISTER_TO'];
    }
    $rsData = CUser::GetList($by, $order, $arFilter);

    $USER_COUNT = IntVal($rsData->SelectedRowsCount());
    if ($USER_COUNT <= 0)
        $errorMessage .= GetMessage("STE_EMPTY_USER") . ".<br>";

    $TRANSACT_DATE = Trim($TRANSACT_DATE);
    if (strlen($TRANSACT_DATE) <= 0)
        $errorMessage .= GetMessage("STE_EMPTY_DATE") . ".<br>";

    $AMOUNT = str_replace(",", ".", $AMOUNT);
    $AMOUNT = DoubleVal($AMOUNT);
    if ($AMOUNT <= 0)
        $errorMessage .= GetMessage("STE_EMPTY_SUM") . ".<br>";

    $CURRENCY = Trim($CURRENCY);
    if (strlen($CURRENCY) <= 0)
        $errorMessage .= GetMessage("STE_EMPTY_CURRENCY") . ".<br>";

    $DEBIT = (($DEBIT == "Y") ? "Y" : "N");

    if (strlen($errorMessage) <= 0) {
        while ($arUser = $rsData->Fetch()) {
            $res = CSaleUserAccount::UpdateAccount(IntVal($arUser['ID']), (($DEBIT == "Y") ? $AMOUNT : -$AMOUNT), $CURRENCY, "MANUAL", IntVal($ORDER_ID), $NOTES);
            if (!$res) {
                if ($ex = $APPLICATION->GetException()) {
                    $errorMessage .= $ex->GetString() . " (" . $arUser['LOGIN'] . "[" . $arUser['ID'] . "])" . ".<br>";
                } else {
                    $errorMessage .= GetMessage("STE_ERROR_SAVE_ACCOUNT") . " (" . $arUser['LOGIN'] . "[" . $arUser['ID'] . "])" . ".<br>";
                }
            }
        }
    }

    if (strlen($errorMessage) <= 0)
        LocalRedirect("/bitrix/admin/sale_transact_admin.php?lang=" . LANG . GetFilterParams("filter_", false));
    else
        $bVarsFromForm = true;
}
if ($bVarsFromForm)
    $DB->InitTableVarsForEdit("b_sale_user_transact", "", "str_");


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sale/prolog.php");

$APPLICATION->SetTitle(GetMessage("STE_TITLE"));

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?
$aMenu = array(
    array(
        "TEXT" => GetMessage("STEN_2FLIST"),
        "TITLE" => GetMessage("STEN_2FLIST_TITLE"),
        "LINK" => "/bitrix/admin/sale_transact_admin.php?lang=" . LANG . GetFilterParams("filter_"),
        "ICON" => "btn_list"
    )
);
$context = new CAdminContextMenu($aMenu);
$context->Show();
?>

<?
if (strlen($errorMessage) > 0)
    echo CAdminMessage::ShowMessage(Array("DETAILS" => $errorMessage, "TYPE" => "ERROR", "MESSAGE" => GetMessage("STE_ERROR"), "HTML" => true));
?>


<form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?" name="form1">
<? echo GetFilterHiddens("filter_"); ?>
    <input type="hidden" name="Update" value="Y">
    <input type="hidden" name="lang" value="<? echo LANG ?>">
<?= bitrix_sessid_post() ?>

    <?
    $aTabs = array(
        array("DIV" => "edit1", "TAB" => GetMessage("STEN_TAB_TRANSACT"), "ICON" => "sale", "TITLE" => GetMessage("STEN_TAB_TRANSACT_DESCR"))
    );

    $tabControl = new CAdminTabControl("tabControl", $aTabs);
    $tabControl->Begin();
    ?>

    <?
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td></td>
        <td id="SEARCH-RESULT">

        </td>
    </tr>
    <tr>
        <td><? echo GetMessage("STE_GENDER") ?></td>
        <td>
            <select name="GENDER" id="GENDER">
                <option value="">(неизвестно)</option>
                <option value="M"<?php
    if (!empty($GENDER) && $GENDER == 'M') {
        echo ' selected';
    }
    ?>>Мужской</option>
                <option value="F"<?php
                        if (!empty($GENDER) && $GENDER == 'F') {
                            echo ' selected';
                        }
                        ?>>Женский</option>
                <option value="N"<?php if (!empty($GENDER) && $GENDER == 'N') {
                    echo ' selected';
                } ?>>Не указан</option>
            </select>
        </td>
    </tr>

    <tr>
        <td><? echo GetMessage("STE_CVP") ?></td>
        <td>
            <select multiple="" name="CVP[]" size="10" id="CVP">
                <option value="" selected="">нет</option>
                <?php foreach ($tmp as $_cvp): ?>
                    <option value="<?php echo $_cvp['ID']; ?>"<?php
                    if (!empty($CVP) && in_array($_cvp['ID'], $CVP)) {
                        echo ' selected';
                    }
                    ?>><?php echo $_cvp['NAME']; ?></option>
                        <?php endforeach; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><? echo GetMessage("STE_REGISTER") ?></td>
        <td>
            <span style="line-height: 30px;">c</span>
            <div class="adm-input-wrap adm-input-wrap-calendar">
                <input class="adm-input adm-input-calendar" type="text" name="DATE_REGISTER_FROM" size="13" value="<?php
                if (!empty($DATE_REGISTER_FROM)) {
                    echo $DATE_REGISTER_FROM;
                }
                ?>" id="DATE_REGISTER_FROM">
                <span class="adm-calendar-icon" title="Нажмите для выбора даты" onclick="BX.calendar({node: this, field: 'DATE_REGISTER_FROM', form: '', bTime: false, bHideTime: false});"></span>
            </div>
            <span style="line-height: 30px;">по</span>
            <div class="adm-input-wrap adm-input-wrap-calendar">
                <input class="adm-input adm-input-calendar" type="text" name="DATE_REGISTER_TO" size="13" value="<?php
                if (!empty($DATE_REGISTER_TO)) {
                    echo $DATE_REGISTER_TO;
                }
                ?>" id="DATE_REGISTER_TO">
                <span class="adm-calendar-icon" title="Нажмите для выбора даты" onclick="BX.calendar({node: this, field: 'DATE_REGISTER_TO', form: '', bTime: false, bHideTime: false});"></span>
            </div>
        </td>
    </tr>





    <tr class="adm-detail-required-field">
        <td><? echo GetMessage("STE_DATE") ?>:</td>
        <td><?
            if (strlen($str_TRANSACT_DATE) <= 0)
                $str_TRANSACT_DATE = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID)));
            echo CalendarDate("TRANSACT_DATE", $str_TRANSACT_DATE, "form1", "20", "");
            ?>
        </td>
    </tr>
    <tr class="adm-detail-required-field">
        <td><? echo GetMessage("STE_SUM") ?></td>
        <td>
            <input type="text" name="AMOUNT" size="10" maxlength="20" value="<?= roundEx($str_AMOUNT, SALE_VALUE_PRECISION) ?>">
        </td>
    </tr>
    <tr>
        <td><? echo GetMessage("STE_CURRENCY") ?></td>
        <td>
            <? echo CCurrency::SelectBox("CURRENCY", $str_CURRENCY, "", false, "", "") ?>
        </td>
    </tr>
    <tr>
        <td><? echo GetMessage("STE_TYPE") ?></td>
        <td>
            <select name="DEBIT">
                <option value="Y"<? if ($str_DEBIT == "Y") echo " selected"; ?>><? echo GetMessage("STE_DEBET") ?></option>
                <option value="N"<? if ($str_DEBIT == "N") echo " selected"; ?>><? echo GetMessage("STE_KREDIT") ?></option>
            </select>
        </td>
    </tr>
    <tr>
        <td valign="top"><? echo GetMessage("STE_ORDER_ID") ?></td>
        <td valign="top">
            <input type="text" name="ORDER_ID" size="5" maxlength="20" value="<?= $str_ORDER_ID ?>">
        </td>
    </tr>
    <tr>
        <td valign="top"><? echo GetMessage("STE_NOTES") ?></td>
        <td valign="top">
            <textarea name="NOTES" rows="3" cols="40"><?= $str_NOTES ?></textarea>
        </td>
    </tr>

    <?
    $tabControl->EndTab();
    ?>

    <?
    $tabControl->Buttons();
    ?>

    <input type="submit" name="save" value="<?= GetMessage("STE_SAVE") ?>" <? if ($saleModulePermissions < "U") echo "disabled" ?>>&nbsp;<input type="button" value="<? echo GetMessage("STE_CANCEL1") ?>" OnClick="window.location = '/bitrix/admin/sale_transact_admin.php?lang=<?= LANG ?><?= GetFilterParams("filter_") ?>'">

    <?
    $tabControl->End();
    ?>

</form>
<script>
    search_users();
    BX.bind(BX('GENDER'), 'change', function() {
        search_users();
    });
    BX.bind(BX('CVP'), 'change', function() {
        search_users();
    });
    BX.bind(BX('DATE_REGISTER_FROM'), 'change', function() {
        search_users();
    });
    BX.bind(BX('DATE_REGISTER_TO'), 'change', function() {
        search_users();
    });

    function search_users() {
        data = {};
        data.GENDER = BX('GENDER').value;
        data.DATE_REGISTER_FROM = BX('DATE_REGISTER_FROM').value;
        data.DATE_REGISTER_TO = BX('DATE_REGISTER_TO').value;
        data.CVP = [];
        CVP = BX('CVP');
        for (var j = 0; j < CVP.options.length; j++)
        {
            if (CVP.options[j].selected && CVP.options[j].value != "")
                data.CVP[data.CVP.length] = CVP.options[j].value;
        }
        BX.ajax.post("/bitrix/admin/sale_transact_edit_group_search.php", data, show_search_result);
    }
    function show_search_result(responce) {
        BX.adjust(BX('SEARCH-RESULT'), {html: responce});
    }


</script>

<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
