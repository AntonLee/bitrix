<?php
$MESS['CHAT_LINK_ENTITY_ID_FIELD'] = 'Уникальный идентификатор';
$MESS['CHAT_LINK_ENTITY_CHAT_ID_FIELD'] = 'Идентификатор чата';
$MESS['CHAT_LINK_ENTITY_SERVICE_ID_FIELD'] = 'ID сервиса';
$MESS['CHAT_LINK_ENTITY_CONF_URL_FIELD'] = 'URL конференции';
$MESS['CHAT_LINK_ENTITY_CONF_EXTERNAL_ID_FIELD'] = 'Внешний ID конфереции';
