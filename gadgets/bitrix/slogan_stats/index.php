<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock"))
	return false;

$arGadgetParams["RND_STRING"] = randString(8);

$allUsers = 0;
$allItems = 0;

$stats = array();

$els = CIBlockElement::GetList(Array(), Array('IBLOCK_ID' => 22, 'ACTIVE' => 'Y'), false, false, Array('ID', 'NAME', 'PROPERTY_BRAND'));

while ($el = $els->Fetch())
{
	$el['COUNT_USERS'] = CIBlockElement::GetList(Array(), Array('IBLOCK_ID' => 23, 'ACTIVE' => 'Y', 'PROPERTY_BRAND' => $el['PROPERTY_BRAND_VALUE']), Array('PROPERTY_USER'))->SelectedRowsCount();
	$el['COUNT_ITEMS'] = CIBlockElement::GetList(Array(), Array('IBLOCK_ID' => 23, 'ACTIVE' => 'Y', 'PROPERTY_BRAND' => $el['PROPERTY_BRAND_VALUE']), Array());
	
	$allItems += $el['COUNT_ITEMS'];
	
	$stats[$el['ID']] = $el;
}

$allUsers = CIBlockElement::GetList(Array(), Array('IBLOCK_ID' => 23, 'ACTIVE' => 'Y'), Array('PROPERTY_USER'))->SelectedRowsCount();

?><script type="text/javascript">
	var gdSaleTabControl_<?=$arGadgetParams["RND_STRING"]?> = false;
</script><?



?><div class="bx-gadgets-tabs-wrap" id="bx_gd_tabset_sale_<?=$arGadgetParams["RND_STRING"]?>"><?

	?><div class="bx-gadgets-tabs-cont"><?

		$countTab = 1;
		for($i = 0; $i < $countTab; $i++)
		{
			?><div id="<?=$aTabs[$i]["DIV"]?>_content" style="display: <?=($i==0 ? "block" : "none")?>;" class="bx-gadgets-tab-container"><?
				if ($i == 0)
				{
					?><table class="bx-gadgets-table">
						<tbody>
							<tr>
								<th>&nbsp;</th>
								<th>Всего</th><?
								
								foreach ($stats AS $stat)
								{
									echo '<th>'.$stat['NAME'].'</th>';
								}
							?></tr>
							<tr>
								<td>Кол-во участников</td>
								<td align="center"><?=$allUsers ?></td>
								<?
								foreach ($stats AS $stat)
								{
									echo '<td align="center">'.$stat['COUNT_USERS'].'</td>';
								}
								?>
							</tr>
							<tr>
								<td>Кол-во слоганов</td>
								<td align="center"><?=$allItems ?></td><?
								
								foreach ($stats AS $stat)
								{
									echo '<td align="center">'.$stat['COUNT_ITEMS'].'</td>';
								}
								
							?></tr>
							
						</tbody>
					</table><?
				}
			?></div><?
		}
	?></div>
</div>
<script type="text/javascript">
	BX.ready(function(){
		gdSaleTabControl_<?=$arGadgetParams["RND_STRING"]?> = new gdTabControl('bx_gd_tabset_sale_<?=$arGadgetParams["RND_STRING"]?>');
	});
</script>