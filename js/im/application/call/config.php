<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	'js' => [
		'./dist/call.bundle.js',
	],
	'css' =>[
		'./dist/call.bundle.css',
	],
	'rel' => [
		'main.polyfill.core',
		'im.application.core',
		'im.lib.utils',
		'im.provider.rest',
		'promise',
		'pull.client',
		'ui.vue',
		'im.lib.logger',
		'im.component.dialog',
		'pull.component.status',
	],
	'skip_core' => true,
];