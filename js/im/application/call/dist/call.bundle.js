this.BX = this.BX || {};
this.BX.Messenger = this.BX.Messenger || {};
(function (exports,im_application_core,im_lib_utils,im_provider_rest,promise,pull_client,ui_vue,im_lib_logger) {
	'use strict';

	/**
	 * Bitrix Im
	 * Application External Call
	 *
	 * @package bitrix
	 * @subpackage im
	 * @copyright 2001-2020 Bitrix
	 */
	ui_vue.Vue.component('bx-im-application-call', {
	  props: {
	    userId: {
	      default: 0
	    },
	    chatId: {
	      default: 0
	    },
	    dialogId: {
	      default: '0'
	    }
	  },
	  methods: {
	    logEvent: function logEvent(name) {
	      for (var _len = arguments.length, params = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	        params[_key - 1] = arguments[_key];
	      }

	      im_lib_logger.Logger.info.apply(im_lib_logger.Logger, [name].concat(params));
	    }
	  },
	  template: "\n\t\t<div class=\"bx-im-application-call\">\n\t\t\t<bx-pull-component-status/>\n\t\t\t<div>UserId: <b>{{userId}}</b></div>\n\t\t\t<div>ChatId: <b>{{chatId}}</b></div>\n\t\t\t<div>DialogId: <b>{{dialogId}}</b></div>\n\t\t</div>\n\t"
	});

	/**
	 * Bitrix Im mobile
	 * Dialog application
	 *
	 * @package bitrix
	 * @subpackage mobile
	 * @copyright 2001-2020 Bitrix
	 */
	var CallApplication =
	/*#__PURE__*/
	function () {
	  /* region 01. Initialize */
	  function CallApplication() {
	    var _this = this;

	    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	    babelHelpers.classCallCheck(this, CallApplication);
	    this.inited = false;
	    this.initPromise = new BX.Promise();
	    this.params = params;
	    this.template = null;
	    this.rootNode = this.params.node || document.createElement('div');
	    this.event = new ui_vue.VueVendorV2();
	    this.initCore().then(function () {
	      return _this.initComponent();
	    }).then(function () {
	      return _this.initComplete();
	    });
	  }

	  babelHelpers.createClass(CallApplication, [{
	    key: "initCore",
	    value: function initCore() {
	      var _this2 = this;

	      return new Promise(function (resolve, reject) {
	        im_application_core.Core.ready().then(function (controller) {
	          _this2.controller = controller;
	          resolve();
	        });
	      });
	    }
	  }, {
	    key: "initComponent",
	    value: function initComponent() {
	      var _this3 = this;

	      console.log('2. initComponent');
	      this.controller.getStore().commit('application/set', {
	        dialog: {
	          chatId: this.getChatId(),
	          dialogId: this.getDialogId()
	        }
	      });
	      return this.controller.createVue(this, {
	        el: this.rootNode,
	        data: function data() {
	          return {
	            userId: _this3.getUserId(),
	            chatId: _this3.getChatId(),
	            dialogId: _this3.getDialogId()
	          };
	        },
	        template: "<bx-im-application-call :userId=\"userId\" :chatId=\"chatId\" :dialogId=\"dialogId\"/>"
	      }).then(function (vue) {
	        _this3.template = vue;
	        return new Promise(function (resolve, reject) {
	          return resolve();
	        });
	      });
	    }
	  }, {
	    key: "initComplete",
	    value: function initComplete() {
	      this.inited = true;
	      this.initPromise.resolve(this);
	    }
	  }, {
	    key: "ready",
	    value: function ready() {
	      if (this.inited) {
	        var promise$$1 = new BX.Promise();
	        promise$$1.resolve(this);
	        return promise$$1;
	      }

	      return this.initPromise;
	    }
	    /* endregion 01. Initialize */

	    /* region 02. Methods */

	  }, {
	    key: "getUserId",
	    value: function getUserId() {
	      var userId = this.getLocalize('USER_ID');
	      return userId ? parseInt(userId) : 0;
	    }
	  }, {
	    key: "getChatId",
	    value: function getChatId() {
	      return this.params.chatId ? parseInt(this.params.chatId) : 0;
	    }
	  }, {
	    key: "getDialogId",
	    value: function getDialogId() {
	      return this.params.chatId ? 'chat' + this.params.chatId.toString() : '0';
	    }
	  }, {
	    key: "getHost",
	    value: function getHost() {
	      return location.origin || '';
	    }
	  }, {
	    key: "getSiteId",
	    value: function getSiteId() {
	      return 's1';
	    }
	    /* endregion 02. Methods */

	    /* region 03. Utils */

	  }, {
	    key: "addLocalize",
	    value: function addLocalize(phrases) {
	      return this.controller.addLocalize(phrases);
	    }
	  }, {
	    key: "getLocalize",
	    value: function getLocalize(name) {
	      return this.controller.getLocalize(name);
	    }
	    /* endregion 03. Utils */

	  }]);
	  return CallApplication;
	}();

	exports.CallApplication = CallApplication;

}((this.BX.Messenger.Application = this.BX.Messenger.Application || {}),BX.Messenger.Application,BX.Messenger.Lib,BX.Messenger.Provider.Rest,BX,BX,BX,BX.Messenger.Lib));
//# sourceMappingURL=call.bundle.js.map
