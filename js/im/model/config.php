<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	'js' => [
		'./dist/registry.bundle.js',
	],
	'rel' => [
		'main.polyfill.core',
		'im.const',
		'ui.vue',
		'ui.vue.vuex',
		'im.lib.utils',
	],
	'skip_core' => true,
];