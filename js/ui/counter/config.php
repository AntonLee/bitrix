<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	"css" => [
		"/bitrix/js/ui/counter/ui.counter.css",
		"/bitrix/js/ui/counter/ui.counter.ie.css"
	],
	"js" => "/bitrix/js/ui/counter/ui.counter.js",
	"rel" => "ui.fonts.opensans"
];