<?
$MESS["EC_LOCATION"] = "Місце";
$MESS["EC_LOCATION_404"] = "немає даних";
$MESS["EC_LOCATION_LABEL"] = "Місце проведення";
$MESS["EC_LOCATION_MEETING_ROOM_SET"] = "Налаштувати список";
$MESS["EC_LOCATION_RESERVE_ERROR"] = "Місцезнаходження зайнято в зазначений час";
$MESS["EC_MAP_LOCATION_LABEL"] = "Вказати місцезнаходження";
$MESS["EC_MEETING_ROOM_ADD"] = "Додати";
$MESS["EC_MEETING_ROOM_LIST_TITLE"] = "Список переговорних кімнат";
$MESS["EC_MEETING_ROOM_PLACEHOLDER"] = "Назва";
$MESS["EC_SEC_SLIDER_SAVE"] = "Зберегти";
$MESS["EC_SEC_SLIDER_CANCEL"] = "Скасувати";
$MESS["EC_SEC_SLIDER_CLOSE"] = "Закрити";

$MESS["EC_DEL_REC_EVENT"] = "Видалити подію, що повторюється";
$MESS["EC_EDIT_REC_EVENT"] = "Змінити подію, що повторюється";
$MESS["EC_REC_EV_ONLY_THIS_EVENT"] = "Тільки це";
$MESS["EC_REC_EV_ALL"] = "Всі повторення";
$MESS["EC_REC_EV_NEXT"] = "Це та всі наступні";
?>