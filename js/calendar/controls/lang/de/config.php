<?
$MESS["EC_LOCATION"] = "Standort";
$MESS["EC_LOCATION_404"] = "Keine Daten";
$MESS["EC_LOCATION_LABEL"] = "Standort";
$MESS["EC_LOCATION_MEETING_ROOM_SET"] = "Liste konfigurieren";
$MESS["EC_LOCATION_RESERVE_ERROR"] = "Der Standort ist zum angegebenen Zeitpunkt nicht verfügbar.";
$MESS["EC_MAP_LOCATION_LABEL"] = "Standort angeben";
$MESS["EC_MEETING_ROOM_ADD"] = "Hinzufügen";
$MESS["EC_MEETING_ROOM_LIST_TITLE"] = "Besprechungsräume";
$MESS["EC_MEETING_ROOM_PLACEHOLDER"] = "Name";
$MESS["EC_SEC_SLIDER_CANCEL"] = "Abbrechen";
$MESS["EC_SEC_SLIDER_CHANGE"] = "ändern";
$MESS["EC_SEC_SLIDER_CLOSE"] = "Schließen";

$MESS["EC_DEL_REC_EVENT"] = "Serientermin löschen";
$MESS["EC_EDIT_REC_EVENT"] = "Serientermin bearbeiten";
$MESS["EC_REC_EV_ALL"] = "Alle Wiederholungen";
$MESS["EC_REC_EV_NEXT"] = "Diesen und alle nächsten";
$MESS["EC_REC_EV_ONLY_THIS_EVENT"] = "Nur diesen Termin";
?>